<?php

namespace App\Tests\functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiEndToEndTest extends WebTestCase
{
    /**
     * @param string $requestQuery
     * @param array $expectedResponse
     * @param int $expectedResponseCode
     * @dataProvider provider_testFindUser
     */
    public function testFindEntity(string $requestQuery, array $expectedResponse, int $expectedResponseCode = 200)
    {
        $client = $this->createClient();
        $client->request('GET', $requestQuery);

        $response = $client->getResponse();
        $actual = json_decode($response->getContent());

        $this->assertEquals($expectedResponseCode, $response->getStatusCode());
        $this->assertNotFalse($actual);
        $this->assertEquals($expectedResponse, (array)$actual);
    }

    public function provider_testFindUser()
    {
        return
            [
                // USERS
                ['/users/dingo',
                    ['name' => '',
                        'url' => 'https://api.github.com/users/dingo',
                        'email' => '',
                        'createdAt' => '2013-06-24T09:00:22+0000'
                    ]
                ],
                ['/users/ThiSUSER_DPES_NOT_EXST', [], 404],

                // REPOSITORIES
                ['/repositories/dingo/api',
                    [
                        'fullName' => 'dingo/api',
                        'description' => 'A RESTful API package for the Laravel and Lumen frameworks.',
                        'cloneUrl' => 'https://github.com/dingo/api.git',
                        'stars' => 8988,
                        'createdAt' => '2014-04-11T12:31:18+0000'
                    ]
                ],
                ['/repositories/ThiSREPO_DPES_NOT_EXST', [], 404],
            ];
    }
}
