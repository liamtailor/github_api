<?php

namespace App\Tests\functional;

use App\Command\GitHubRepositoriesCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class RepositoriesCommandEndToEndTest extends KernelTestCase
{
    /**
     * @var Application
     */
    private $application;

    public function setUp(): void
    {
        $kernel = static::createKernel();
        $this->application = new Application($kernel);
    }

    /**
     * @dataProvider provider_testExecuteRepositories
     * @param string $username
     * @param string $repositoryName
     * @param array $expected
     */
    public function testExecuteRepositories(string $username, string $repositoryName, array $expected)
    {
        $command = $this->application->find('app:repositories');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                GitHubRepositoriesCommand::USERNAME_ARGUMENT_NAME => $username,
                GitHubRepositoriesCommand::REPOSITORY_ARGUMENT_NAME => $repositoryName,
            ]
        );

        $output = $commandTester->getDisplay();
        foreach ($expected as $expectedLine) {
            $this->assertStringContainsString($expectedLine, $output);
        }
    }

    public function testExecuteRepositories_WithNonExistingUsernameAndRepositoryName()
    {
        $username = 'NONEXSTNGT_USER_NAME1234';
        $repositoryName = 'NONEXSTNGT_REPOSTRY_NAME1234';
        $command = $this->application->find('app:repositories');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                GitHubRepositoriesCommand::USERNAME_ARGUMENT_NAME => $username,
                GitHubRepositoriesCommand::REPOSITORY_ARGUMENT_NAME => $repositoryName,
            ]
        );
        $expected = 'Repository ' . $username . '/' . $repositoryName . ' NOT FOUND';

        $output = $commandTester->getDisplay();

        $this->assertStringContainsString($expected, $output);
    }

    public function provider_testExecuteRepositories()
    {
        return
            [
                [
                    'dingo',
                    'api',
                    [
                        'fullName' => 'dingo/api',
                        'description' => 'A RESTful API package for the Laravel and Lumen frameworks.',
                        'cloneUrl' => 'https://github.com/dingo/api.git',
                        'stars' => 8988,
                        'createdAt' => '2014-04-11T12:31:18+0000'
                    ]
                ],
            ];
    }
}
