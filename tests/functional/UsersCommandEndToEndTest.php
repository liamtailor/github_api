<?php

namespace App\Tests\functional;

use App\Command\GitHubUsersCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class UsersCommandEndToEndTest extends KernelTestCase
{
    /**
     * @var Application
     */
    private $application;

    public function setUp(): void
    {
        $kernel = static::createKernel();
        $this->application = new Application($kernel);
    }

    /**
     * @dataProvider provider_testExecuteUsers
     * @param string $username
     * @param array $expected
     */
    public function testExecuteUsers(string $username, array $expected)
    {
        $command = $this->application->find('app:users');
        $commandTester = new CommandTester($command);
        $commandTester->execute([GitHubUsersCommand::USERNAME_ARGUMENT_NAME => $username]);

        $output = $commandTester->getDisplay();
        foreach ($expected as $expectedLine) {
            $this->assertStringContainsString($expectedLine, $output);
        }
    }

    public function testExecuteUsers_WithNonExistingUsername()
    {
        $username = 'NON_EXSTNG_USSERNAMEE112';
        $command = $this->application->find('app:users');
        $commandTester = new CommandTester($command);
        $commandTester->execute([GitHubUsersCommand::USERNAME_ARGUMENT_NAME => $username]);
        $expected = 'User ' . $username . ' NOT FOUND';

        $output = $commandTester->getDisplay();

        $this->assertStringContainsString($expected, $output);
    }

    public function provider_testExecuteUsers()
    {
        return
            [
                ['dingo',
                    ['name' => '',
                        'url' => 'https://api.github.com/users/dingo',
                        'email' => '',
                        'createdAt' => '2013-06-24T09:00:22+0000'
                    ]
                ]
            ];
    }
}
