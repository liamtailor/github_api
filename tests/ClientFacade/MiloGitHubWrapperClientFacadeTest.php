<?php

namespace App\Tests\unit\ClientFacade;

use App\ClientFacade\MiloGitHubWrapperClientFacade;
use App\ClientFacade\MiloWrapperResponseHandler;
use App\ClientFacade\TokenFactory;
use Milo\Github\Api;
use Milo\Github\Http\Request;
use Milo\Github\Http\Response;
use PHPUnit\Framework\TestCase;

class MiloGitHubWrapperClientFacadeTest extends TestCase
{

    public function testGetRepository_ReturnsValidJsonOnSuccessfulRequest()
    {
        $json = '{"fullName":"dingo\/api","description":"A RESTful API package for the Laravel and Lumen frameworks.","cloneUrl":"https:\/\/github.com\/dingo\/api.git","stars":8988,"createdAt":"2014-04-11T12:31:18+0000"}';
        $expected = (array)json_decode($json);
        $username = 'dingo';
        $repository = 'api';
        $request = $this->createMock(Request::class);
        $response = $this->createMock(Response::class);
        $response->method('getCode')->willReturn(200);
        $response->method('getContent')->willReturn($json);
        $responseHandler = $this->createMock(MiloWrapperResponseHandler::class);
        $responseHandler->method('handleResponse')->with($response)->willReturn($expected);
        $api = $this->createMock(Api::class);
        $api->method('createRequest')->with('GET', '/repos/' . $username . '/' . $repository)->willReturn($request);
        $api->method('request')->with($request)->willReturn($response);
        $tokenFactory = $this->createMock(TokenFactory::class);
        $client = new MiloGitHubWrapperClientFacade($api, $tokenFactory, $responseHandler);


        $actual = $client->getRepository($username, $repository);

        $this->assertEquals($expected, $actual);
    }

    public function testGetUser_ReturnsValidJsonOnSuccessfulRequest()
    {
        $json = '{"name":"","url":"https:\/\/api.github.com\/users\/dingo","email":"","createdAt":"2013-06-24T09:00:22+0000"}';
        $expected = (array)json_decode($json);
        $username = 'dingo';
        $request = $this->createMock(Request::class);
        $response = $this->createMock(Response::class);
        $response->method('getCode')->willReturn(200);
        $response->method('getContent')->willReturn($json);
        $responseHandler = $this->createMock(MiloWrapperResponseHandler::class);
        $responseHandler->method('handleResponse')->with($response)->willReturn($expected);
        $api = $this->createMock(Api::class);
        $api->method('createRequest')->with('GET', '/users/' . $username)->willReturn($request);
        $api->method('request')->with($request)->willReturn($response);
        $tokenFactory = $this->createMock(TokenFactory::class);
        $client = new MiloGitHubWrapperClientFacade($api, $tokenFactory, $responseHandler);
        $actual = $client->getUser($username);

        $this->assertEquals($expected, $actual);
    }
}
