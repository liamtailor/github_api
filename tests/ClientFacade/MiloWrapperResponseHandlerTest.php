<?php

namespace App\Tests\ClientFacade;

use App\ClientFacade\MiloWrapperResponseHandler;
use App\Exceptions\ApiRateLimitExceededException;
use App\Exceptions\BadGitHubApiTokenException;
use App\Exceptions\UnrecognizedGitHubResponse;
use Milo\Github\Http\Response;
use PHPUnit\Framework\TestCase;

class MiloWrapperResponseHandlerTest extends TestCase
{
    /**
     * @var MiloWrapperResponseHandler
     */
    private $responseHandler;

    public function setUp(): void
    {
        $this->responseHandler = new MiloWrapperResponseHandler();
    }

    public function testHandleResponse_ReturnsDecodedContentsOn200ResponseCode()
    {
        $json = '{"test":"value"}';
        $response = $this->createMock(Response::class);
        $response->method('getCode')->willReturn(200);
        $response->method('getContent')->willReturn($json);
        $expected = (array)json_decode($json);

        $actual = $this->responseHandler->handleResponse($response);

        $this->assertEquals($expected, $actual);
    }

    public function testHandleResponse_ReturnsEmptyArrayOn404ResponseCode()
    {
        $response = $this->createMock(Response::class);
        $response->method('getCode')->willReturn(404);
        $expected = [];

        $actual = $this->responseHandler->handleResponse($response);

        $this->assertEquals($expected, $actual);
    }

    public function testHandleResponse_ThrowsApiRateLimitExceededExceptionOn403ResponseCode()
    {
        $response = $this->createMock(Response::class);
        $response->method('getCode')->willReturn(403);

        $this->expectException(ApiRateLimitExceededException::class);

        $this->responseHandler->handleResponse($response);
    }

    public function testHandleResponse_ThrowsBadGitHubApiTokenExceptionOn401ResponseCode()
    {
        $response = $this->createMock(Response::class);
        $response->method('getCode')->willReturn(401);

        $this->expectException(BadGitHubApiTokenException::class);

        $this->responseHandler->handleResponse($response);
    }

    public function testHandleResponse_ThrowsUnrecognizedGitHubResponseOnUnhandledResponseCode()
    {
        $response = $this->createMock(Response::class);
        $response->method('getCode')->willReturn(1111);

        $this->expectException(UnrecognizedGitHubResponse::class);

        $this->responseHandler->handleResponse($response);
    }

    public function testHandleResponse_ThrowsUnrecognizedGitHubResponseOnInvalidJsonInResponse()
    {
        $json = 'MALFORMED_JSON';
        $response = $this->createMock(Response::class);
        $response->method('getCode')->willReturn(200);
        $response->method('getContent')->willReturn($json);

        $this->expectException(UnrecognizedGitHubResponse::class);

        $this->responseHandler->handleResponse($response);
    }
}
