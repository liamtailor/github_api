<?php


namespace App\Exceptions;


use Throwable;

class ApiRateLimitExceededException extends GitHubApiException
{
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        $message = $message ?: 'API rate limit exceeded.';
        parent::__construct($message, $code, $previous);
    }
}