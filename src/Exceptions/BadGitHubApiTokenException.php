<?php


namespace App\Exceptions;


use Throwable;

class BadGitHubApiTokenException extends GitHubApiException
{
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        $message = $message ?: 'Invalid GitHubApi OAuth token given.';
        parent::__construct($message, $code, $previous);
    }
}