<?php


namespace App\Exceptions;


use Throwable;

class MissingGitHubApiResponseParametersKeysException extends GitHubApiException
{
    /**
     * MissingGitHubApiResponseParametersKeysException constructor.
     * @param array $missingKeys
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(array $missingKeys, int $code = 0, Throwable $previous = null)
    {
        parent::__construct('Missing keys in data returned from GitHub API repos endpoint: ' . implode(',', $missingKeys), $code, $previous);
    }
}