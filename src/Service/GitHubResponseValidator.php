<?php


namespace App\Service;


use App\Exceptions\MissingGitHubApiResponseParametersKeysException;

class GitHubResponseValidator
{
    /**
     * @param array $requiredKeys
     * @param array $gitHubApiResponseEntity
     * @throws MissingGitHubApiResponseParametersKeysException
     */
    public function validate(array $requiredKeys, array $gitHubApiResponseEntity)
    {
        $missingKeys = [];
        foreach ($requiredKeys as $requiredKey){
            if (!array_key_exists($requiredKey, $gitHubApiResponseEntity)){
                $missingKeys[] = $requiredKey;
            }
        }
        if (count($missingKeys) > 0){
            throw new MissingGitHubApiResponseParametersKeysException($missingKeys);
        }
    }
}