<?php


namespace App\Service;


use App\Exceptions\GitHubApiException;
use App\Model\GitHubRepository;
use App\Model\GitHubUser;

interface IGitHubApiService
{
    /**
     * @param string $ownerLogin
     * @param string $repositoryName
     * @return GitHubRepository|null
     * @throws GitHubApiException
     */
    public function getRepository(string $ownerLogin, string $repositoryName): ?GitHubRepository;

    /**
     * @param string $login
     * @return GitHubUser|null
     * @throws GitHubApiException
     */
    public function getUser(string $login): ?GitHubUser;
}