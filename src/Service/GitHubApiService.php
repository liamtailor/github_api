<?php

namespace App\Service;

use App\ClientFacade\IClientFacade;
use App\Model\GitHubRepository;
use App\Model\GitHubRepositoryFactory;
use App\Model\GitHubUser;
use App\Model\GitHubUserFactory;

class GitHubApiService implements IGitHubApiService
{
    /**
     * @var IClientFacade
     */
    private $client;
    /**
     * @var GitHubUserFactory
     */
    private $gitHubUserFactory;
    /**
     * @var GitHubRepositoryFactory
     */
    private $gitHubRepositoryFactory;

    public function __construct(IClientFacade $client, GitHubUserFactory $gitHubUserFactory, GitHubRepositoryFactory $gitHubRepositoryFactory)
    {
        $this->client = $client;
        $this->gitHubUserFactory = $gitHubUserFactory;
        $this->gitHubRepositoryFactory = $gitHubRepositoryFactory;
    }


    /**
     * @inheritDoc
     */
    public function getRepository(string $ownerLogin, string $repositoryName): ?GitHubRepository
    {
        $repository = $this->client->getRepository($ownerLogin, $repositoryName);
        if (empty($repository)) {
            return null;
        }
        $repository = $this->gitHubRepositoryFactory->make($repository);

        return $repository;
    }

    /**
     * @inheritDoc
     */
    public function getUser(string $login): ?GitHubUser
    {
        $user = $this->client->getUser($login);
        if (empty($user)) {
            return null;
        }
        $user = $this->gitHubUserFactory->make($user);

        return $user;
    }
}