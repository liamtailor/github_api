<?php


namespace App\Command;

use App\Command\OutputPrinters\IRepositoryPrinter;
use App\Exceptions\GitHubApiException;
use App\Service\IGitHubApiService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GitHubRepositoriesCommand extends Command
{
    const USERNAME_ARGUMENT_NAME = 'username';
    const REPOSITORY_ARGUMENT_NAME = 'repository';

    protected static $defaultName = 'app:repositories';
    /**
     * @var IGitHubApiService
     */
    private $gitHubApiService;
    /**
     * @var IRepositoryPrinter
     */
    private $repositoryPrinter;

    public function __construct(IGitHubApiService $gitHubApiService, IRepositoryPrinter $repositoryPrinter)
    {
        parent::__construct();
        $this->gitHubApiService = $gitHubApiService;
        $this->repositoryPrinter = $repositoryPrinter;
    }

    protected function configure()
    {
        $this
            ->setDescription('Find a GitHub user by username given as first argument.')
            ->addArgument(self::USERNAME_ARGUMENT_NAME, InputArgument::REQUIRED, 'GitHub username')
            ->addArgument(self::REPOSITORY_ARGUMENT_NAME, InputArgument::REQUIRED, 'GitHub repository name');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument(self::USERNAME_ARGUMENT_NAME);
        $repositoryName = $input->getArgument(self::REPOSITORY_ARGUMENT_NAME);
        try {
            $repository = $this->gitHubApiService->getRepository($username, $repositoryName);
            $this->repositoryPrinter->print($output, $repository, $username, $repositoryName);
            return Command::SUCCESS;
        } catch (GitHubApiException $e) {
            $output->writeln('Api service error: ' . $e->getMessage());
            return Command::FAILURE;
        }
    }
}