<?php


namespace App\Command\OutputPrinters;


use App\Model\GitHubRepository;
use Symfony\Component\Console\Output\OutputInterface;

class LineByLineRepositoryPrinter implements IRepositoryPrinter
{

    /**
     * @inheritDoc
     */
    public function print(OutputInterface $output, ?GitHubRepository $repository, string $username, string $repositoryName): void
    {
        if (is_null($repository)) {
            $output->writeln('Repository ' . $username . '/' . $repositoryName . ' NOT FOUND');
        } else {
            $output->writeln('fullName: ' . $repository->getFullName());
            $output->writeln('description: ' . $repository->getDescription());
            $output->writeln('cloneUrl: ' . $repository->getCloneUrl());
            $output->writeln('stars: ' . $repository->getStars());
            $output->writeln('createdAt: ' . $repository->getCreatedAt()->format(DATE_ISO8601));
        }
    }
}