<?php


namespace App\Command\OutputPrinters;


use App\Model\GitHubUser;
use Symfony\Component\Console\Output\OutputInterface;

class LineByLineUserPrinter implements IUserPrinter
{

    /**
     * @inheritDoc
     */
    public function print(OutputInterface $output, ?GitHubUser $user, string $username): void
    {
        if (is_null($user)) {
            $output->writeln('User ' . $username . ' NOT FOUND');
        } else {
            $output->writeln('name: ' . $user->getName());
            $output->writeln('url: ' . $user->getUrl());
            $output->writeln('email: ' . $user->getEmailAddress());
            $output->writeln('createdAt: ' . $user->getCreatedAt()->format(DATE_ISO8601));
        }
    }
}