<?php


namespace App\Command\OutputPrinters;


use App\Model\GitHubUser;
use Symfony\Component\Console\Output\OutputInterface;

interface IUserPrinter
{
    /**
     * @param OutputInterface $output
     * @param GitHubUser|null $user
     * @param string $username
     */
    public function print(OutputInterface $output, ?GitHubUser $user, string $username): void;
}