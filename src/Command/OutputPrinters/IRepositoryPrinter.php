<?php


namespace App\Command\OutputPrinters;


use App\Model\GitHubRepository;
use Symfony\Component\Console\Output\OutputInterface;

interface IRepositoryPrinter
{
    /**
     * @param OutputInterface $output
     * @param GitHubRepository|null $repository
     * @param string $username
     * @param string $repositoryName
     */
    public function print(OutputInterface $output, ?GitHubRepository $repository, string $username, string $repositoryName): void;
}