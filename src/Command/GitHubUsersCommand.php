<?php


namespace App\Command;

use App\Command\OutputPrinters\IUserPrinter;
use App\Exceptions\GitHubApiException;
use App\Service\IGitHubApiService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GitHubUsersCommand extends Command
{
    const USERNAME_ARGUMENT_NAME = 'username';

    protected static $defaultName = 'app:users';
    /**
     * @var IGitHubApiService
     */
    private $gitHubApiService;
    /**
     * @var IUserPrinter
     */
    private $userPrinter;

    public function __construct(IGitHubApiService $gitHubApiService, IUserPrinter $userPrinter)
    {
        parent::__construct();
        $this->gitHubApiService = $gitHubApiService;
        $this->userPrinter = $userPrinter;
    }

    protected function configure()
    {
        $this
            ->setDescription('Find a GitHub user by username given as first argument.')
            ->addArgument(self::USERNAME_ARGUMENT_NAME, InputArgument::REQUIRED, 'GitHub username');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument(self::USERNAME_ARGUMENT_NAME);
        try {
            $user = $this->gitHubApiService->getUser($username);
            $this->userPrinter->print($output, $user, $username);
            return Command::SUCCESS;
        } catch (GitHubApiException $e) {
            $output->writeln('Api service error: ' . $e->getMessage());
            return Command::FAILURE;
        }
    }
}