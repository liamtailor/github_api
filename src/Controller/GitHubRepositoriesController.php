<?php

namespace App\Controller;

use App\Exceptions\GitHubApiException;
use App\Service\IGitHubApiService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GitHubRepositoriesController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/repositories/{ownerLogin}/{repositoryName}", methods={"GET"}, requirements={"ownerLogin"="\w+", "repositoryName"="\w+"}, name="github_repositories")
     * @param string $ownerLogin
     * @param string $repositoryName
     * @param IGitHubApiService $gitHubApiService
     * @return Response
     */
    public function index(string $ownerLogin, string $repositoryName, IGitHubApiService $gitHubApiService)
    {
        try {
            $repository = $gitHubApiService->getRepository($ownerLogin, $repositoryName);
        } catch (GitHubApiException $e) {
            $this->logger->error($e->getMessage());
            return new JsonResponse(['Error: ' => $e->getMessage()], 500);
        }
        if (is_null($repository)) {
            return new JsonResponse([], 404);
        } else {
            return new JsonResponse($repository);
        }
    }
}