<?php

namespace App\Controller;

use App\Exceptions\GitHubApiException;
use App\Service\IGitHubApiService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GitHubUserController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/users/{login}", methods={"GET"}, requirements={"login"="\w+"}, name="github_users")
     * @param string $login
     * @param IGitHubApiService $gitHubApiService
     * @return Response
     */
    public function index(string $login, IGitHubApiService $gitHubApiService)
    {
        try {
            $user = $gitHubApiService->getUser($login);
        } catch (GitHubApiException $e) {
            $this->logger->error($e->getMessage());
            return new JsonResponse(['Error: ' => $e->getMessage()], 500);
        }
        if (is_null($user)) {
            return new JsonResponse([], 404);
        } else {
            return new JsonResponse($user);
        }
    }
}