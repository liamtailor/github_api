<?php

namespace App\ClientFacade;

use App\Exceptions\ApiRateLimitExceededException;
use App\Exceptions\UnrecognizedGitHubResponse;

interface IClientFacade
{
    /**
     * @param string $username
     * @param string $repository
     * @return array|string
     * @throws UnrecognizedGitHubResponse
     * @throws ApiRateLimitExceededException
     */
    public function getRepository(string $username, string $repository);

    /**
     * @param string $username
     * @return array|string
     * @throws UnrecognizedGitHubResponse
     * @throws ApiRateLimitExceededException
     */
    public function getUser(string $username);
}