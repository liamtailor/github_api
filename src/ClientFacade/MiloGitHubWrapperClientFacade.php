<?php

namespace App\ClientFacade;

use App\Exceptions\ApiRateLimitExceededException;
use App\Exceptions\BadGitHubApiTokenException;
use App\Exceptions\UnrecognizedGitHubResponse;
use Milo\Github\Api;

class MiloGitHubWrapperClientFacade implements IClientFacade
{
    /**
     * @var Api
     */
    private $apiClient;
    /**
     * @var MiloWrapperResponseHandler
     */
    private $responseHandler;

    public function __construct(Api $apiClient, TokenFactory $tokenFactory, MiloWrapperResponseHandler $responseHandler)
    {
        $this->responseHandler = $responseHandler;
        $this->apiClient = $apiClient;
        $token = $tokenFactory->make();
        if ($token) {
            $this->apiClient->setToken();
        }
    }

    /**
     * @param string $username
     * @param string $repository
     * @return array|string
     * @throws UnrecognizedGitHubResponse
     * @throws ApiRateLimitExceededException
     * @throws BadGitHubApiTokenException
     */
    public function getRepository(string $username, string $repository)
    {
        $request = $this->apiClient->createRequest('GET', '/repos/' . rawurlencode($username) . '/' . rawurlencode($repository));
        $response = $this->apiClient->request($request);

        return $this->responseHandler->handleResponse($response);
    }

    /**
     * @param string $username
     * @return array|string
     * @throws UnrecognizedGitHubResponse
     * @throws ApiRateLimitExceededException
     * @throws BadGitHubApiTokenException
     */
    public function getUser(string $username)
    {
        $request = $this->apiClient->createRequest('GET', '/users/' . rawurlencode($username));
        $response = $this->apiClient->request($request);

        return $this->responseHandler->handleResponse($response);
    }
}