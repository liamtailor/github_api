<?php

namespace App\ClientFacade;

use Milo\Github\OAuth\Token;

class TokenFactory
{
    /**
     * @var string
     */
    private $token = '';

    public function __construct()
    {
        if (array_key_exists('GITHUB_OAUTH_TOKEN', $_ENV)) { // Token is optional. API works without it, just with a reduced rate limit.
            $this->token = $_ENV['GITHUB_OAUTH_TOKEN'];
        }
    }

    /**
     * @return Token|null
     */
    public function make(): ?Token
    {
        if (strlen($this->token) > 0) {
            return new Token($this->token);
        } else {
            return null;
        }
    }
}