<?php


namespace App\ClientFacade;


use App\Exceptions\ApiRateLimitExceededException;
use App\Exceptions\BadGitHubApiTokenException;
use App\Exceptions\UnrecognizedGitHubResponse;
use Milo\Github\Http\Response;

class MiloWrapperResponseHandler
{
    /**
     * @param Response $response
     * @return array|mixed
     * @throws UnrecognizedGitHubResponse
     * @throws ApiRateLimitExceededException
     * @throws BadGitHubApiTokenException
     */
    public function handleResponse(Response $response)
    {
        $responseCode = $response->getCode();
        switch ($responseCode) {
            case 200:
                $entity = json_decode($response->getContent());
                if (is_null($entity)) {
                    throw new UnrecognizedGitHubResponse('Response contents could not be decoded from json.');
                } else {
                    return (array)$entity;
                }
            case 403:
                throw new ApiRateLimitExceededException();
            case 404:
                return [];
            case 401:
                throw new BadGitHubApiTokenException();
            default:
                throw new UnrecognizedGitHubResponse('Unrecognized response code: ' . $responseCode . '. Response message: ' . $response->getContent());
        }
    }
}