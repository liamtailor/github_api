<?php


namespace App\Model;


use DateTime;
use JsonSerializable;

class GitHubUser implements JsonSerializable
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $emailAddress;
    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * GitHubUser constructor.
     * @param string $name
     * @param string $url
     * @param string $emailAddress
     * @param DateTime $createdAt
     */
    public function __construct(string $name, string $url, string $emailAddress, DateTime $createdAt)
    {
        $this->name = $name;
        $this->url = $url;
        $this->emailAddress = $emailAddress;
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }


    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'url' => $this->getUrl(),
            'email' => $this->getEmailAddress(),
            'createdAt' => $this->getCreatedAt()->format(DATE_ISO8601),
        ];
    }
}