<?php


namespace App\Model;

use DateTime;
use JsonSerializable;

class GitHubRepository implements JsonSerializable
{
    /**
     * @var string
     */
    private $fullName;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $cloneUrl;
    /**
     * @var int
     */
    private $stars;
    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * GitHubRepository constructor.
     * @param string $fullName
     * @param string $description
     * @param string $cloneUrl
     * @param int $stars
     * @param DateTime $createdAt
     */
    public function __construct(string $fullName, string $description, string $cloneUrl, int $stars, DateTime $createdAt)
    {
        $this->fullName = $fullName;
        $this->description = $description;
        $this->cloneUrl = $cloneUrl;
        $this->stars = $stars;
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCloneUrl(): string
    {
        return $this->cloneUrl;
    }

    /**
     * @return int
     */
    public function getStars(): int
    {
        return $this->stars;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function jsonSerialize()
    {
        return [
          'fullName' => $this->getFullName(),
          'description' => $this->getDescription(),
          'cloneUrl' => $this->getCloneUrl(),
          'stars' => $this->getStars(),
          'createdAt' => $this->getCreatedAt()->format(DATE_ISO8601)
        ];
    }
}