<?php


namespace App\Model;


use App\Exceptions\MissingGitHubApiResponseParametersKeysException;
use App\Exceptions\UnrecognizedGitHubResponseParameterFormatException;
use App\Service\GitHubResponseValidator;
use DateTime;

class GitHubUserFactory
{
    private const REQUIRED_KEYS = ['name', 'url', 'email', 'created_at'];
    /**
     * @var GitHubResponseValidator
     */
    private $gitHubResponseValidator;

    public function __construct(GitHubResponseValidator $gitHubResponseValidator)
    {
        $this->gitHubResponseValidator = $gitHubResponseValidator;
    }

    /**
     * @param array $user
     * @return GitHubUser
     * @throws UnrecognizedGitHubResponseParameterFormatException
     * @throws MissingGitHubApiResponseParametersKeysException
     */
    public function make(array $user)
    {
        $this->gitHubResponseValidator->validate(self::REQUIRED_KEYS, $user);

        $dateFormat = DATE_ISO8601;
        $createdDateTime = DateTime::createFromFormat($dateFormat, $user['created_at']);
        if ($createdDateTime === false) {
            throw new UnrecognizedGitHubResponseParameterFormatException('Expected date format ' . $dateFormat . ' got ' . $user['created_at']);
        }
        $user = new GitHubUser(
            $user['name'] ?? '',
            $user['url'] ?? '',
            $user['email'] ?? '',
            $createdDateTime
        );

        return $user;
    }
}