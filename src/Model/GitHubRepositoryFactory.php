<?php


namespace App\Model;


use App\Exceptions\MissingGitHubApiResponseParametersKeysException;
use App\Service\GitHubResponseValidator;
use DateTime;
use DateTimeInterface;

class GitHubRepositoryFactory
{
    private const REQUIRED_KEYS = ['full_name', 'description', 'clone_url', 'stargazers_count', 'created_at'];
    /**
     * @var GitHubResponseValidator
     */
    private $gitHubResponseValidator;

    public function __construct(GitHubResponseValidator $gitHubResponseValidator)
    {
        $this->gitHubResponseValidator = $gitHubResponseValidator;
    }

    /**
     * @param array $repository
     * @return GitHubRepository
     * @throws MissingGitHubApiResponseParametersKeysException
     */
    public function make(array $repository)
    {
        $this->gitHubResponseValidator->validate(self::REQUIRED_KEYS, $repository);

        $createdDateTime = DateTime::createFromFormat(DateTimeInterface::ISO8601, $repository['created_at']);
        $repository = new GitHubRepository(
            $repository['full_name'] ?? '',
            $repository['description'] ?? '',
            $repository['clone_url'] ?? '',
            $repository['stargazers_count'] ?? '',
            $createdDateTime
        );

        return $repository;
    }
}