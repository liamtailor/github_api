Token OAuth standardowo nie był by zawarty w pliku ".env" jako że tokeny/hasła/itp. nie powinny być przechowywane w repozytorium. Tutaj zrobiłem wyjątek dla wygody.

Jeśli chodzi o stress test (20 jednoczesnych zapytań na sekundę), to z tego co mi wiadomo stress-testy na localhoście nie mają żadnego sensu (jako że prędkość w dużej mierze może zależeć od serwera)
Ciężko mi więc określić, czy ten wymóg jest spełniony.

Analiza snapshot'a profilowania XDebug wykazuje, że najwięcej czasu zajmuje includowanie plików Autoloaderem (~50ms) - co na serwerze produkcyjnym załatwia cache - oraz "php::curl_exec" (~100-200ms), czyli prędkość komunikacji z API GitHub przez Internet.
Poza tym 10 funkcji zajmuje od 5 do 1 ms, a rezta poniżej 1ms. 
Oczywiście sam XDebug również znacznie spowalnia działanie serwera lokalnego, więc te wyniki nie są wymierne.   

Do deployu wystarczy:
    "docker-compose up" (jeśli mamy Dockera)
    LUB
    "composer install && symfony server:start"

Dostępne endpointy:
GET .../repositories/{owner-login}/{repository-name}
GET .../users/{login}